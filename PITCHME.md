# Simplon Prod.tn
+++?color=#301115
## Sommaire : 
Slide 1  : Besoins et Objectifs de Simplon Prod Tunisie. <br/>
Slide 2  : Cible du site web. <br/>
Slide 3  : Budget. <br/>
Slide 3  : Arborescence du site web Simplon Prod Tunisie. <br/>
Slide 4  : Maquette de Simplon Prod Tunisie. <br/>
Slide 5  : Contenu Rédigé. <br/>
Slide 6  : Mise en ligne du site web et hébergement

---?color=#9b7479

## Besoins et Objectifs de Simplon Prod Tunisie

			 				
Lors de cette étape on répond à la question “à quoi votre site Web servira-t-il? “ <br/>
				
Besoin 1 : Promouvoir Simplon Prod Tunisie en ligne. <br/>
Besoin 2 : Acquérir de nouveaux clients via le nouveau site web. <br/>
Besoin 3 : Faciliter  le travail de l’équipe Simplon Prod Tunisie  en présentant les services, les lieux et les moyens de contact. <br/>
Besoin 4 :  Diffuser de l'information générale, de l’actualité sur une promotion, <br/>
Besoin 5 : Développer des partenariats avec des entreprises et des associations et organismes publics. 


+++?color=#314221
## Choix du Public Cible 
Lors de cette étape on répond à la question : “Qui voulez-vous atteindre avec votre site Web et dans quelle(s) langue(s)?” <br/>
			
Cible 1 : des clients potentiels, des startups, des associations et des entreprises.
Cible 2 : des futurs partenaires.
Cible 3 : des fournisseurs.
Cible 4: les acteurs du l'écosystème qui ont une orientation sociales et solidaires.

---?color=#a233c6
## : Détermination du budget global de réalisation
Le budget prends en considération les éléments suivants :
Type de langage utilisé 
La rémunération des développeurs.
Les frais d’hébergement, de mise à jour et maintenance.

Le budget est souvent à prévoir avant le lancement du pro

+++?color=#82464c
## Elaboration de l’arborescence du site web	 		
						
À cette étape, on structure une architecture efficace de contenu du site  web pour aider notre client Simplon Prod Tunisie à bien réaliser ses objectifs.

---?color=#c6b1b3

## Conception de la maquette du site Internet 
			
L'esthétique d’un site  web reflète la philosophie de l'entreprise propriétaire.
La visiteurs consultent votre site web pour faire une opinion à votre sujet et pour prendre une décision.

+++?color=#c67c43

## Création du contenu	
			
Simplonprodtunisie.co est un site à vocation commerciale. 
Un style prometteur bref et clair est à adapté lors de la rédaction. 
Le contenus doit être attractif.
Les informations présentées aux visiteurs doivent êtres clairs, pertinentes , rapide d'accès et suscitant l'intérêt.

---?color=#cc6874
## Mise en ligne et hébergement du site web
			 			
Il faut choisir un nom de domaine et le réserver puis l'héberger chez un hébergeur web.
Proposition 1 : simplonprodtunisie.com
Proposition 2 : simplonprodtunisie.tn
Proposition 3 : simplonprodtunisie.co
Proposition 4 : simplonprod.tn

+++?color=#c1cef2

## Promouvoir le site web avec un référencement optimal 	 			
			
La création d’un site web doit être accompagnée d’un stratégie de référencement web réussi afin de faciliter aux internaute la recherche et l'accès à votre site dans les premiers résultat de recherche.
Ceci permettra de développer la crédibilité de l’entreprise et de présenter une offre commerciale compétitive.

---

## Développement de l’identité numérique du site  			
Après la création du site web, son hébergement et son référencement, il est important d'établir un contact réel avec la clientèle intéressée.
Un plan d’action précis et clair permettra d'identifier les canaux d'accès à l’audience choisit, le type de contenu à transmettre, la fréquence d’interaction avec la cible, les valeurs, les causes et les activités à partager avec cette cible ainsi que la personne responsable de maintenir son intérêt.




## Add Some Slide Candy

![](assets/img/presentation.png)

---
@title[Customize Slide Layout]

@snap[west span-50]
## thank you 
